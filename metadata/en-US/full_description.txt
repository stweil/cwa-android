This is a fork of the official German <a href="https://www.coronawarn.app/en/">Corona-Warn-App</a> (CWA) without proprietary dependencies. While the app itself is Free Software, it depends on Google’s proprietary Exposure Notification Framework. This fork instead uses the drop-in compatible microg implementation. CCTG is fully compatible with the official app, including recording and sending tracing data as well as test submission and result collection.

<b>Tracing corona infections.</b> For contact tracing, the app collects short-lived IDs via bluetooth and broadcasts its own. In case of an infection, it allows to upload ones own IDs to a server using a TAN, allowing contacts to notice a match with their collected IDs and to be notified. All that happens without storing more data than necessary.

<b>Check in.</b> Another way of warning at-risk persons is by checking in to events and locations using CCTG. Hosts may use the app to generate a QR code that guests can scan. In case of an infection, users who were checked in at the same time or arrived shortly after the infected person left can be warned.

Additionally, the app shows statistic cards regarding the rate of infections in Germany and provides a contact journal for manually entering people one has met.

<b>Transnational exposure logging.</b> Users outside of Germany can use the app and be warned about exposures in <a href="https://www.coronawarn.app/en/faq/#interoperability_countries">participating countries</a>, but they won't be able to retrieve test results through the app. In case of an infection, they may <a href"https://www.coronawarn.app/en/faq/#test_in_other_country">call the TAN hotline</a> and enter the TAN within the app to warn their contacts.

<b>Lollipop support.</b> Android 5 is supported, however, Bluetooth drivers on older devices might not support a required feature for full support; see <a href="https://altbeacon.github.io/android-beacon-library/beacon-transmitter-devices.html">this list</a> for whether an old device supports this app. Not fully compatible devices will show a note on the main screen.
