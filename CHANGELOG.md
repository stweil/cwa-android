### 2.2.1.0

* Create rapid test profiles to share your data with at test points more quickl (CWA)
* Various bug fixes and improvements (CWA)

### 2.1.2.0

* Register Rapid Antigen Tests (CWA)
* Negative RAT proof (CWA)
* Display international phone numbers (CWA)

### 2.0.5.0

* Friendly error message when scanning invalid code (CWA)
* Better networking behavior (CWA)

### 2.0.3.0

* Check-in selection in submission flow (CWA)
* Fix white/black screen when adding a new person to the contact diary
* Some more minor bug fixes

### 2.0.2.0-rc1

* Add event check-in functionality via QR codes (CWA)
* Create QR codes for your event from within the app (CWA)
* Add a warning card on the main screen when battery optimizations are enabled
* Fixed some more visual glitches on Android 5
* Reduce apk size by not prerendering all vector graphics

### 1.15.1.0

* Add Switzerland to transnational exposure logging (upstream CWA)
* Adjust risk tiles (upstream CWA)

### 1.14.3.0

* Contact journal can now record additional information (duration, etc.) (upstream CWA)
* Add contact journal launcher shortcut (upstream CWA)
* Some more details about the risk status are now displayed (upstream CWA)
* Show encounters bottom tab icon for external microG (pending [upstream support](https://github.com/microg/GmsCore/pull/1416))

### 1.13.2.3

* Properly detect external/internal microG version on /e/ ROM

### 1.13.2.2

* Remove faulty GMS detection code.

### 1.13.2.1

* Fix a crash occurring on startup on some devices.

### 1.13.2.0

* New tab based navigation on the home screen for accessing the contact journal and the exposure history (only with builtin microG)
* Exposure logging can now be turned back on again after a positive test. (upstream CWA)
* Reworked microG detection code, opening the correct microG instance (internal vs external) should now always work.
* Show a warning on the home screen in case your phone isn't fully compatible with contact tracing.
* Enable debug log recording functionality in production builds (can be found in App Information).

**NOTE:** Upstream CWA's data donation and survey features won't be available in CCTG until further notice ([CWA Wishlist ticket](https://github.com/corona-warn-app/cwa-wishlist/issues/356))

### 1.12.0.0

* Display exposure history in the contact journal (upstream CWA)
* Release information screen in the app (upstream CWA)
* Lots of minor bug fixes (see [upstream changelog](https://github.com/corona-warn-app/cwa-app-android/releases/tag/v1.12.0) for details)

### 1.11.0.8

* reworked main screen (upstream CWA)
* new statistics cards (upstream CWA)
* reworked collected IDs chart (upstream microg)
* translucent status bar

### 1.10.1.1

* Make exposure notification settings screen more accessible (Thanks @pr0gr8mm3r !)

### 1.10.1.0

* Fix a bug where you could get stuck on the launcher activity after upgrading microG
* Add Bulgarian Translation for the donation card
* Add German FAQ page (linked when the app is used in German)
* Changes from CWA 1.10.1:
  * Implement a Contact Diary
  * Fix days since last encounter being displayed incorrectly

### 1.9.1.6

* Fix microG version check on /e/ ROMs (/e/ has two possible microG versions, one with ENF and one without. The one without ENF will now be correctly ignored.)

### 1.9.1.5

* Ignore disabled microG versions in the version check
* Set the minimum microG version to 0.2.15 (0.2.16 has additional fixes but it not strictly required)

### 1.9.1.4

* New microg version with some stability fixes
* Check for latest microG version and prompt to update it
* Link to our own FAQ from the App

### 1.9.1.3

* fix crash when opening microG ENF settings
* fix crash during exposure check
* some translation fixes (added polish translation of our additions)

### 1.9.1.2

* Lots of upstream changes, most importantly switching to Exposure Window mode (ENF v2)
* No more gray cards and minimum tracing time. The app will report a risk right away now
* Make the microG UI for exposures available (through the ENF version number in App Information)
* Allow exporting the exposure db for analysis in another app
* Fix for exposure checks sometimes taking very long
* Directly ask for battery optimization exception instead of just sending to settings
* Android 5 support

### 1.7.1.2

* Rebuild for F-Droid reproducible builds

### 1.7.1.1
Initial release of Corona Contact Tracing Germany
